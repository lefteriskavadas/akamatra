<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="colored-zone">
<div class="container">
<ul class="latestnews<?php echo $moduleclass_sfx; ?> cards-container">
<?php foreach ($list as $item) : ?>
	<li itemscope itemtype="https://schema.org/Article">
		<a href="<?php echo $item->link; ?>" itemprop="url" class="card">
			<div class="card__header">
				<div class="card__media">
					<?php
					$item->jcfields = FieldsHelper::getFields('com_content.article', $item, true);
					$image = '';
					foreach($item->jcfields as $field)
					{
						if($field->id == 1) {
							$image = FieldsHelper::render('com_content.article', 'field.render', array('field'=> $field));
						}
					}
					 ?>
					<?php echo $image; ?>
				</div>
			</div>
			<div class="card__wrapper">
				<div class="card__content">
					<div class="card__tag-wrapper <?php echo 'card__tag-wrapper--'.$item->category_alias; ?>">
						<span class="card__triangle"></span>
						<p class="card__tag"><?php echo $item->category_title; ?></p>
						<span class="card__triangle--flipped"></span>
					</div>
					<h4 class="card__title">
						<span class="card__underline" itemprop="name"><?php echo $item->title; ?></span>
					</h4>
				</div>
			</div>
		</a>
	</li>
<?php endforeach; ?>
<li class="grid-sizer"></li>
</ul>
</div>
</div>
