<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_finder
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\String\StringHelper;

// Get the mime type class.
$mime = !empty($this->result->mime) ? 'mime-' . $this->result->mime : null;

$show_description = $this->params->get('show_description', 1);

if ($show_description)
{
	// Calculate number of characters to display around the result
	$term_length = StringHelper::strlen($this->query->input);
	$desc_length = $this->params->get('description_length', 255);
	$pad_length  = $term_length < $desc_length ? (int) floor(($desc_length - $term_length) / 2) : 0;

	// Make sure we highlight term both in introtext and fulltext
	if (!empty($this->result->summary) && !empty($this->result->body))
	{
		$full_description = FinderIndexerHelper::parse($this->result->summary . $this->result->body);
	}
	else
	{
		$full_description = $this->result->description;
	}

	// Find the position of the search term
	$pos = $term_length ? StringHelper::strpos(StringHelper::strtolower($full_description), StringHelper::strtolower($this->query->input)) : false;

	// Find a potential start point
	$start = ($pos && $pos > $pad_length) ? $pos - $pad_length : 0;

	// Find a space between $start and $pos, start right after it.
	$space = StringHelper::strpos($full_description, ' ', $start > 0 ? $start - 1 : 0);
	$start = ($space && $space < $pos) ? $space + 1 : $start;

	$description = JHtml::_('string.truncate', StringHelper::substr($full_description, $start), $desc_length, true);
}

$route = $this->result->route;

// Get the route with highlighting information.
if (!empty($this->query->highlight)
	&& empty($this->result->mime)
	&& $this->params->get('highlight_terms', 1)
	&& JPluginHelper::isEnabled('system', 'highlight'))
{
	$route .= '&highlight=' . base64_encode(json_encode($this->query->highlight));
}
$this->result->jcfields = FieldsHelper::getFields('com_content.article', $this->result, true);
$this->result->image = '';
foreach($this->result->jcfields as $field)
{
  if($field->id == 1) {
    $this->result->image = FieldsHelper::render('com_content.article', 'field.render', array('field'=> $field));
  }
}

$parts = explode(':', $this->result->catslug);
$this->result->categoryAlias = $parts[1];
?>
<li>
  <a href="<?php echo JRoute::_($route); ?>" class="card">
    <div class="card__header">
      <div class="card__media">
        <?php echo $this->result->image; ?>
      </div>
    </div>
    <div class="card__wrapper">
      <div class="card__content">
        <div class="card__tag-wrapper <?php echo 'card__tag-wrapper--'.$this->result->categoryAlias; ?>">
          <span class="card__triangle"></span>
          <p class="card__tag"><?php echo $this->result->category; ?></p>
          <span class="card__triangle--flipped"></span>
        </div>
        <h4 class="card__title">
          <span class="card__underline"><?php echo $this->result->title; ?></span>
        </h4>
      </div>
    </div>
  </a>
</li>
