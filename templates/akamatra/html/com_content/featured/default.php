<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

JHtml::_('behavior.caption');

// If the page class is defined, add to class as suffix.
// It will be a separate class if the user starts it with a space
?>
<div class="colored-zone">
<div class="container blog-featured<?php echo $this->pageclass_sfx; ?>" itemscope itemtype="https://schema.org/Blog">
<?php if ($this->params->get('show_page_heading') != 0) : ?>
<div class="page-header">
	<h1>
	<?php echo $this->escape($this->params->get('page_heading')); ?>
	</h1>
</div>
<?php endif; ?>



<ul class="cards-container layout-1-2">
<?php foreach ($this->items as $key => $item) : $item->link = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catid, $item->language)); ?>
  <li itemscope itemtype="https://schema.org/Article" <?php if($key == 0): ?> class="standout-article" <?php endif; ?>>
    <a href="<?php echo $item->link; ?>" itemprop="url" class="card">
      <div class="card__header">
        <div class="card__media">
          <?php echo FieldsHelper::render('com_content.article', 'field.render', array('field'=> $item->jcfields[1])); ?>
        </div>
      </div>
      <div class="card__wrapper">
        <div class="card__content">
          <div class="card__tag-wrapper <?php echo 'card__tag-wrapper--'.$item->category_alias; ?>">
            <span class="card__triangle"></span>
            <p class="card__tag"><?php echo $item->category_title; ?></p>
            <span class="card__triangle--flipped"></span>
          </div>
          <h4 class="card__title">
            <span class="card__underline" itemprop="name"><?php echo $item->title; ?></span>
          </h4>
        </div>
      </div>
    </a>
  </li>
<?php endforeach; ?>
</ul>



<?php if ($this->params->def('show_pagination', 2) == 1  || ($this->params->get('show_pagination') == 2 && $this->pagination->pagesTotal > 1)) : ?>
	<div class="pagination">

		<?php if ($this->params->def('show_pagination_results', 1)) : ?>
			<p class="counter pull-right">
				<?php echo $this->pagination->getPagesCounter(); ?>
			</p>
		<?php endif; ?>
				<?php echo $this->pagination->getPagesLinks(); ?>
	</div>
<?php endif; ?>

</div>
</div>
