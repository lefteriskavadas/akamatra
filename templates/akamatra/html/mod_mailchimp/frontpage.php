<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2018 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

JHtml::_('jquery.framework');
?>
<div class="container">
<div id="homepage-newsletter">
<h4>Join a couple of my friends and my mom for a monthly newsletter full of crafts and ideas!</h4>
<form action="<?php echo JRoute::_('index.php?option=com_ajax&module=mailchimp&method=subscribe&format=json'); ?>" id="module-<?php echo $module->id; ?>-mailchimp-subscribe">
  <div class="input-with-button">
    <input aria-label="Enter your email" type="email" name="email" required="required" placeholder="Enter your email" />
    <button class="btn newsletter-form__btn" type="submit">SIGN UP</button>
  </div>
  <input type="hidden" name="option" value="com_ajax" />
  <input type="hidden" name="module" value="mailchimp" />
  <input type="hidden" name="method" value="subscribe" />
  <input type="hidden" name="format" value="json" />
  <input type="hidden" name="id" value="<?php echo $module->id; ?>" />
</form>
</div>
<div id="module-<?php echo $module->id; ?>-mailchimp-subscribe-response"></div>
</div>
<script>
jQuery(document).ready(function() {
  jQuery('#module-<?php echo $module->id; ?>-mailchimp-subscribe').on('submit', function(event) {
    event.preventDefault();
    jQuery('#mailchimp-subscribe-response2').empty();
    var form = jQuery(this);
    jQuery.ajax({
      type: 'POST',
      url: form.attr('action'),
      data : form.serialize(),
      dataType: 'json'
    }).done(function(response) {
      jQuery('#module-<?php echo $module->id; ?>-mailchimp-subscribe-response').text(response.message);
    });
  });
});
</script>
