<?php
/**
 * @author      Lefteris Kavadas
 * @copyright   Copyright (c) 2016 - 2018 Lefteris Kavadas / firecoders.com
 * @license     GNU General Public License version 3 or later
 */
defined('_JEXEC') or die;

JHtml::_('jquery.framework');
?>

<form action="<?php echo JRoute::_('index.php?option=com_ajax&module=mailchimp&method=subscribe&format=json'); ?>" id="mailchimp-subscribe">
  <div class="input-with-button">
    <input aria-label="Enter your email" type="email" name="email" required="required" placeholder="Enter your email" />
    <button class="btn newsletter-form__btn" type="submit">SIGN UP</button>
  </div>
  <input type="hidden" name="option" value="com_ajax" />
  <input type="hidden" name="module" value="mailchimp" />
  <input type="hidden" name="method" value="subscribe" />
  <input type="hidden" name="format" value="json" />
  <input type="hidden" name="id" value="<?php echo $module->id; ?>" />
</form>
<div id="mailchimp-subscribe-response"></div>

<script>
jQuery(document).ready(function() {
  jQuery('#mailchimp-subscribe').on('submit', function(event) {
    event.preventDefault();
    jQuery('#mailchimp-subscribe-response').empty();
    var form = jQuery(this);
    jQuery.ajax({
      type: 'POST',
      url: form.attr('action'),
      data : form.serialize(),
      dataType: 'json'
    }).done(function(response) {
      jQuery('#mailchimp-subscribe-response').text(response.message);
    });
  });
});
</script>
