<?php
/**
 * @version		1.0.0
 * @package		Firecoders
 * @author		Lefteris Kavadas
 * @copyright	Copyright (c) 2014 Lefteris Kavadas. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

defined('_JEXEC') or die;
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<base href="https://www.akamatra.com/" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, user-scalable=0, initial-scale=1.0" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="distribution" content="global" />
	<meta name="msapplication-config" content="/templates/akamatra/images/icons/browserconfig.xml" />
	<meta name="theme-color" content="#ffffff" />
	<meta name="description" content="Something went wrong!" />
	<title><?php echo $this->title; ?> <?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8'); ?></title>
	<link href="/templates/akamatra/images/icons/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180" />
	<link href="/templates/akamatra/images/icons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png" />
	<link href="/templates/akamatra/images/icons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png" />
	<link href="/templates/akamatra/images/icons/manifest.json" rel="manifest" />
	<link href="/templates/akamatra/images/icons/site.webmanifest" rel="manifest" />
	<link href="/templates/akamatra/images/icons/safari-pinned-tab.svg" rel="mask-icon" color="#5bbad5" />
	<link href="/templates/akamatra/images/icons/favicon.ico" rel="shortcut icon" />
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&amp;subset=greek" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/style.css" rel="stylesheet" />
</head>
<body>

	<div style="display: flex; flex-direction: column; justify-content: center; align-items: center; max-width: 1000px; margin:80px auto; padding: 20px; text-align: center; background: rgb(255, 240, 235); ">
		<header>
			<div>
				<a id="logo" href="<?php echo JUri::root(false); ?>">
					<img src="<?php echo JUri::root(true).'/templates/'.$this->template; ?>/images/akamatra-logo.png" alt="Akamatra logo"/>
				</a>
			</div>
		</header>
		<div style="margin-top: 40px;">
			<h1 style="font-size: 40px; "><?php echo $this->error->getCode(); ?></h1>
			<h2><?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8');?></h2>
			<p><?php echo JText::_('JERROR_LAYOUT_PAGE_NOT_FOUND'); ?></p>
			<ul class="tags inline">
				<li class="tag-57 tag-list0" itemprop="keywords">
					<a class="hero-button" href="<?php echo JURI::root(); ?>">Home Page</a>
				</li>
			</ul>
		</div>
	</div>

	<?php if ($this->debug) : ?>
		<div class="container">
			<?php echo $this->renderBacktrace(); ?>
			<?php // Check if there are more Exceptions and render their data as well ?>
			<?php if ($this->error->getPrevious()) : ?>
				<?php $loop = true; ?>
				<?php // Reference $this->_error here and in the loop as setError() assigns errors to this property and we need this for the backtrace to work correctly ?>
				<?php // Make the first assignment to setError() outside the loop so the loop does not skip Exceptions ?>
				<?php $this->setError($this->_error->getPrevious()); ?>
				<?php while ($loop === true) : ?>
					<p><strong><?php echo JText::_('JERROR_LAYOUT_PREVIOUS_ERROR'); ?></strong></p>
					<p><?php echo htmlspecialchars($this->_error->getMessage(), ENT_QUOTES, 'UTF-8'); ?></p>
					<?php echo $this->renderBacktrace(); ?>
					<?php $loop = $this->setError($this->_error->getPrevious()); ?>
				<?php endwhile; ?>
				<?php // Reset the main error object to the base error ?>
				<?php $this->setError($this->error); ?>
			<?php endif; ?>
		</div>
	<?php endif; ?>
</body>
</html>
