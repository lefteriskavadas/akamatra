<?php
/**
 * @version		1.0.0
 * @package		Akamatra
 * @author		Lefteris Kavadas
 * @copyright	Copyright (c) 2018 Lefteris Kavadas. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

defined('_JEXEC') or die;

JHtml::_('jquery.framework');
$document = JFactory::getDocument();
$this->language  = $document->language;
$this->direction = $document->direction;
$document->setMetaData('X-UA-Compatible', 'IE=edge', true);
$document->setMetaData('viewport', 'width=device-width, user-scalable=0, initial-scale=1.0');
$document->setMetaData('apple-mobile-web-app-capable', 'yes');
$document->setMetaData('distribution', 'global');
$document->addStyleSheet('https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&amp;subset=greek');
$document->addStyleSheet('templates/'.$this->template.'/css/style.css');
$document->addScript('templates/'.$this->template.'/js/masonry.pkgd.min.js');
$document->addScript('templates/'.$this->template.'/js/script.js');
$document->addHeadLink(JUri::root(true).'/templates/'.$this->template.'/images/icons/apple-touch-icon.png', 'apple-touch-icon', 'rel', array('sizes' => '180x180'));
$document->addHeadLink(JUri::root(true).'/templates/'.$this->template.'/images/icons/favicon-32x32.png', 'icon', 'rel', array('sizes' => '32x32', 'type' => 'image/png'));
$document->addHeadLink(JUri::root(true).'/templates/'.$this->template.'/images/icons/favicon-16x16.png', 'icon', 'rel', array('sizes' => '16x16', 'type' => 'image/png'));
$document->addHeadLink(JUri::root(true).'/templates/'.$this->template.'/images/icons/manifest.json', 'manifest', 'rel');
$document->addHeadLink(JUri::root(true).'/templates/'.$this->template.'/images/icons/site.webmanifest', 'manifest', 'rel');
$document->addHeadLink(JUri::root(true).'/templates/'.$this->template.'/images/icons/safari-pinned-tab.svg', 'mask-icon', 'rel', array('color' => '#5bbad5'));
$document->addHeadLink(JUri::root(true).'/templates/'.$this->template.'/images/icons/favicon.ico', 'shortcut icon', 'rel');
$document->setMetaData('msapplication-config', JUri::root(true).'/templates/'.$this->template.'/images/icons/browserconfig.xml');
$document->setMetaData('theme-color', '#ffffff');
//$document->setMetaData('fb:pages', '1986274191599266', 'property');
//$document->setMetaData('fb:app_id', '1630406617018225', 'property');
//$document->addStyleSheet('https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css');
$document->addScript('https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js');
$document->addScriptDeclaration('
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#fff0eb",
      "text": "#333333"
    },
    "button": {
      "background": "#f48473",
      "text": "#ffffff"
    }
  },
  "position": "bottom-right",
  "content": {
    "dismiss": "OK",
    "href": "https://www.akamatra.com/privacy-policy.html"
  }
});
');
$document->setMetaData('google-site-verification', '-y2sl-7WCBL7xQmTU7cAsFQCjhHvZP2Vgga-Ykc30LM');
//$document->setMetaData('msvalidate.01', 'F8A8E80827BA853CE2D02740158713F5');


$application = JFactory::getApplication();
$option = $application->input->getCmd('option');
$view = $application->input->getCmd('view');
$id = $application->input->getInt('id');
$Itemid = $application->input->getInt('Itemid');
$catid = $application->input->getInt('catid');
$context = $option.'-'.$view;
$menu = $application->getMenu();
$active = $menu->getActive();
$default = $menu->getDefault();
$frontpage = ($active && $active->id == $default->id);
$alias = $active->alias;

if($frontpage)
{
  $document->setMetadata('og:title', $this->getTitle(), 'property');
  $document->setMetadata('og:description', $this->getDescription(), 'property');
  $document->setMetadata('og:url', 'https://www.akamatra.com', 'property');
  $document->setMetadata('og:type', 'website', 'property');
  $document->setMetadata('og:image', 'https://www.akamatra.com/templates/akamatra/images/og-image.jpg', 'property');
}

$logo = 'akamatra-logo.png';
$icons = array('books', 'craft', 'decor', 'ecolife', 'food', 'ideas', 'parenting', 'travel', 'various');
if(in_array($alias, $icons)) {
  $logo = 'akamatra-'.$alias.'.png';
}

$document->addHeadLink('https://akamatra-1.disqus.com', 'preconnect');
$document->addHeadLink('https://links.services.disqus.com', 'preconnect');
$document->addHeadLink('https://www.google-analytics.com', 'preconnect');

?>
<!DOCTYPE html>
<html xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<jdoc:include type="head" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-62284127-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-62284127-1');
</script>

</head>
<body class="menu-item-<?php echo $alias; ?>">
  <?php include 'icons.php'; ?>

  <?php if($this->countModules('header')): ?>
  <div id="header">
    <div class="container">
      <div id="header-nav">
      <button aria-label="Toggle menu and search" id="menu-button_1-0" class="comp menu-button button btn--bare header__menu-btn js-menu-trigger btn popover-toggler">
        <svg class="btn__icon btn__hamburger"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-hamburger"></use></svg>
      </button>

      <a id="logo" href="<?php echo JUri::root(false); ?>">
        <img class="logo-icon" src="<?php echo JUri::root(true).'/templates/'.$this->template; ?>/images/akamatra-logo-icon.png" alt="Akamatra logo"/>
        <img class="logo-text" src="<?php echo JUri::root(true).'/templates/'.$this->template; ?>/images/<?php echo $logo; ?>" width="600" height="106" alt="Akamatra logo"/>
      </a>


      <div id="title"><?php $title = $this->getTitle(); $parts = explode('-', $title); array_pop($parts); $title = implode('-', $parts); echo $title; ?></div>

      <div class="social-share">
        <?php if($view == 'article'): ?>
        <a href="https://www.facebook.com/sharer.php?u=<?php echo urlencode(JUri::current());  ?>"
        onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;"
        class="share-link share-link-facebook" title="Share on Facebook">
        <img alt="Facebook icon" class="share-icon" width="40" height="40" src="<?php echo JUri::root(true).'/templates/'.$this->template; ?>/images/icons/facebook.png" />
        </a>
        <a class="share-link share-link-pinterest" title="Share on Pinterest" data-pin-do="buttonBookmark" data-pin-custom="true" href="https://www.pinterest.com/pin/create/button/?url=<?php echo JUri::current(); ?>&amp;media=&amp;description=<?php echo $this->getTitle(); ?>"><img alt="Pinterest icon" width="40" height="40" class="share-icon" src="<?php echo JUri::root(true).'/templates/'.$this->template; ?>/images/icons/pinterest.png" /></a>
        <?php /*<a href="mailto:?subject=<?php echo 'Shared from akamatra.com: '.$this->getTitle(); ?>&amp;body=<?php echo $this->getDescription().' Read more: '.JUri::current(); ?>" class="share-link share-link-email" title="Email this article">
          <svg class="icon icon-envelope"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-envelope"></use></svg>
        </a>*/ ?>
      <?php else: ?>
        <a href="https://www.facebook.com/AkamatraCraft" target="_blank" rel="noreferrer noopener" class="share-link share-link-facebook" title="Follows us on Facebook">
          <img alt="Facebook icon" class="share-icon" src="<?php echo JUri::root(true).'/templates/'.$this->template; ?>/images/icons/facebook-coloured.png" />
        </a>
        <a href="https://www.pinterest.com/akamatra/" target="_blank" rel="noreferrer noopener" class="share-link share-link-facebook" title="Follows us on Pinterest">
          <img alt="Pinterest icon" class="share-icon" src="<?php echo JUri::root(true).'/templates/'.$this->template; ?>/images/icons/pinterest-coloured.png" />
        </a>
        <a href="https://www.instagram.com/akamatra/" target="_blank" rel="noreferrer noopener" class="share-link share-link-facebook" title="Follows us on Instagram">
          <img alt="Instagram icon" class="share-icon" src="<?php echo JUri::root(true).'/templates/'.$this->template; ?>/images/icons/instagram-coloured.png" />
        </a>
        <a href="https://twitter.com/Akamatras" target="_blank" rel="noreferrer noopener" class="share-link share-link-facebook" title="Follows us on Twitter">
          <img alt="Twitter icon" class="share-icon" src="<?php echo JUri::root(true).'/templates/'.$this->template; ?>/images/icons/twitter-coloured.png" />
        </a>

      <?php endif; ?>
      </div>


      <button aria-label="Toggle menu and search" id="search-button_1-0" class="comp search-button button btn--bare header__search-btn js-search-trigger btn popover-toggler">
        <svg class="btn__icon btn__search"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search"></use></svg>
      </button>

      </div>

    </div>
    <div id="moto"><img src="<?php echo JUri::root(true).'/templates/'.$this->template; ?>/images/moto.jpg" width="294" height="17" alt="Akamatra - Always making something"/></div>

  </div>
  <div id="popover">
    <div class="container">
      <jdoc:include type="modules" name="header" style="html5" />
    </div>
  </div>
  <?php endif; ?>

  <div class="<?php echo $frontpage || $view == 'category' ? 'container-fluid' : 'container'; ?>" id="content">

    <div id="main" class="<?php echo $this->countModules('sidebar') && $view == 'article' ? 'has-sidebar' : ''; ?>">
      <jdoc:include type="component" />
      <?php if($frontpage): ?>
      <jdoc:include type="modules" name="frontpage" style="html5" />
      <?php endif; ?>
    </div>
    <?php if($this->countModules('sidebar') && $view == 'article'): ?>
    <div id="sidebar"><jdoc:include type="modules" name="sidebar" style="html5" /></div>
    <?php endif; ?>
    <div class="clr"></div>
  </div>

  <?php if($view == 'article'): ?>
  <jdoc:include type="modules" name="after-article" style="html5" />
  <?php endif; ?>

  <div id="footer">
    <div class="container">
      <div class="footer-row">
      <jdoc:include type="modules" name="footer" style="html5" />
    </div>
    <div class="credits">Made with <svg class="icon icon-heart"><use xlink:href="#icon-heart"></use></svg> by <a href="https://www.firecoders.com" target="_blank" rel="noopener">Firecoders</a></div>

    </div>
  </div>
  <?php if($view == 'article'): ?>
  <script src="https://www.dwin2.com/pub.452021.min.js"></script>
  <?php endif; ?>
</body>
</html>
