jQuery(document).ready(function() {
  jQuery('.popover-toggler').on('click', function(event) {
    event.preventDefault();
    jQuery('body').toggleClass('popover-opened');
  });
  jQuery('ul.latestnews').masonry({
    itemSelector: 'ul.latestnews li',
    columnWidth: '.grid-sizer',
    percentPosition: true
  });
});


window.addEventListener('scroll', function()  {

  if(document.body.className.indexOf('popover-opened') === -1) {

    if(window.pageYOffset >= 10) {
      if(document.body.className.indexOf('header-sticky') === -1) {
        document.body.className += ' header-sticky';
      }
    } else {
      document.body.className = document.body.className.replace(new RegExp('header-sticky', 'g'), '');
    }

  }
});
